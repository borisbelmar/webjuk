Webjuk
---

Webhook service for easy nodejs deploys on cloud servers.

**Version**: 0.1.0

This API is for deploy nodejs apps using webhooks.

## Requirements
- pm2
- node > 12.x
- yarn
- git (Just gitlab for now)

## Endpoints

**`POST`:** /deploy/:projectName?secretkey=SUPER_SECRET

It will launch a deploy using your config file.

## Env variables

### CONFIG_PATH
**default**: `~`

Where is the config file, user's home folder by default.

### CONFIG_FILENAME
**default**: `.webjuk.conf.json`

Config's file name.

### PM2_ECOSYSTEM_PATH
**default**: `~/ecosystem.config.json`

Ecosystem for pm2 management

### PORT
**default**: `4000`

## Config file

Is important to setup a config file for your projects. By default it must be on ~/.webjuk.config.json

### Example JSON
```json
{
  "version": "0.1.0",
  "baseDir": "/home/boris/projects",
  "secretKey": "SUPER_SECRET_TOKEN",
  "projects": [
    {
      "name": "strapi-blog",
      "user": "user-for-deploy-token",
      "token": "deploy-token",
      "repo": "user/repository",
      "branch": "develop",
      "type": "strapi",
      "env": {
        "NODE_ENV": "production",
        "PORT": "9999"
      }
    },
    {
      "name": "website",
      "user": "user-for-deploy-token",
      "token": "deploy-token",
      "repo": "user/repository",
      "branch": "develop",
      "type": "gatsby",
      "env": {
        "NODE_ENV": "production",
        "STRAPI_URL": "https://localhost:9999"
      },
      "webConfig": {
        "staticDir": "/var/www/html/mywebsite.com"
      }
    }
  ]
}
```

### TypeEnum

This list corresponds to supported project's types:

- strapi
- gatsby

### JSON Properties

**version (string):** For versioning your webjuk config.

**baseDir (string):** Where your apps will exists.

**secretKey (string):** The key for deploys using as query string on endpoint *?secretkey=secret*.

**projects (Project[]):** All your project information.

**Project.name (string):** Your project's name.

**Project.type (TypeEnum):** Your project's type, see TypeEnum.

**Project.repo (string):** Your repo direction. Ex: gitlab.com/`repo`.git

**Project.branch (string):** The branch for deployment. `master` by default.

**Project.user (string):** Only for private gitlab repositories. Deploy token's user.

**Project.token (string):** Only for private gitlab repositories. Deploy token.

**Project.env ({key, value}):** Object with the environment variables for build and run.

**Project.webConfig (WebConfig):** For static websites only. Static configuration.

**WebConfig.staticDir (string):** Set the static dir where your statics files will be moved on. The folder must exists and be setted up separately with nginx or another software for serving it.
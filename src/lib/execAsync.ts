import { exec, ExecOptions } from 'child_process'

function execAsync (cmd: string, options?: ExecOptions) : Promise<string> {
  return new Promise((resolve, reject) => {
    exec(cmd, options || {}, (error, stdout, stderr) => {
      if (error) {
        console.error(error)
        reject(stderr)
      }
      resolve(stdout)
    })
  })
}

export default execAsync

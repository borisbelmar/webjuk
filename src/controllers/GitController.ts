import { existsSync } from 'fs'
import simpleGit, { SimpleGit } from 'simple-git'
import execAsync from '../lib/execAsync'
import { Project } from '../models'

export default class GitController {
  private project: Project

  private localPath: string

  private git: SimpleGit

  public constructor (project: Project, localPath: string) {
    this.project = project
    this.localPath = localPath
    this.git = simpleGit()
  }

  public async cloneProject () : Promise<void> {
    const cloneUrl = this.project.user && this.project.token
      ? `https://${this.project.user}:${this.project.token}@gitlab.com/${this.project.repo}.git`
      : `https://gitlab.com/${this.project.repo}.git`
    try {
      await this.git.clone(cloneUrl, this.localPath, ['-b', this.project.branch || 'master'])
    } catch (e) {
      throw new Error(`Error cloning ${this.project.name} repository. Check your project config.`)
    }
  }

  public async pullProject () : Promise<void> {
    try {
      await execAsync('git pull', { cwd: this.localPath })
    } catch (e) {
      throw new Error('Error pulling repository, check your config file')
    }
  }

  public async cloneOrPullProject () : Promise<void> {
    const repoExists = existsSync(this.localPath)
    if (repoExists) {
      await this.pullProject()
    } else {
      await this.cloneProject()
    }
  }
}

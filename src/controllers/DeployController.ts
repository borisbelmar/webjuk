import { existsSync, readFileSync, writeFileSync } from 'fs'
import { forEachObjIndexed, isEmpty, reject } from 'ramda'
import { copy, emptyDir } from 'fs-extra'
import execAsync from '../lib/execAsync'
import { Project, EcosystemFile } from '../models'
import getUserHome from '../utils/getUserHome'

export default class DeployController {
  private project: Project

  private localPath: string

  public constructor (project: Project, localPath: string) {
    this.project = project
    this.localPath = localPath
  }

  private async buildStrapiProject (): Promise<void> {
    try {
      await execAsync('bash -c yarn', { cwd: this.localPath })
      await execAsync('NODE_ENV=production bash -c \'yarn build\'', { cwd: this.localPath })
    } catch (e) {
      console.log(e.message)
      throw new Error(`Error building the project ${this.project.name}, check your configuration`)
    }
  }

  private async deployPm2Project (): Promise<void> {
    const PM2_ECOSYSTEM_PATH = (
      process.env.PM2_ECOSYSTEM_PATH || `${getUserHome()}/ecosystem.config.json`
    )
    const pm2App = {
      name: this.project.name,
      script: 'npm',
      args: 'start',
      cwd: this.localPath,
      env: this.project.env
    }
    try {
      if (existsSync(PM2_ECOSYSTEM_PATH)) {
        const pm2EcosystemBuffer = readFileSync(PM2_ECOSYSTEM_PATH)
        const pm2EcosystemJSON: EcosystemFile = JSON.parse(String(pm2EcosystemBuffer))
        const updatedApps = reject(app => app.name === this.project.name, pm2EcosystemJSON.apps)
        const updatedEcosystem = {
          apps: [
            ...updatedApps,
            pm2App
          ]
        }
        writeFileSync(PM2_ECOSYSTEM_PATH, JSON.stringify(updatedEcosystem, null, '\t'))
        await execAsync(`pm2 restart ${PM2_ECOSYSTEM_PATH} --update-env --only ${this.project.name}`)
      } else {
        const pm2EcosystemJson: EcosystemFile = {
          apps: [
            pm2App
          ]
        }
        writeFileSync(PM2_ECOSYSTEM_PATH, JSON.stringify(pm2EcosystemJson, null, '\t'))
        await execAsync(`pm2 start ${PM2_ECOSYSTEM_PATH}`)
      }
    } catch (e) {
      throw new Error(`Error deploying the project using pm2 ${this.project.name}, check your configuration`)
    }
  }

  private async buildGatsbyProject () {
    try {
      await execAsync('bash -c yarn', { cwd: this.localPath })
      let envs = ''
      forEachObjIndexed((value, key) => {
        envs += !isEmpty(value) ? `${key}=${value} ` : ''
      }, this.project.env)
      await execAsync(`NODE_ENV=production ${envs} bash -c 'yarn build'`, { cwd: this.localPath })
    } catch (e) {
      throw new Error(`Error building the project ${this.project.name}, check your configuration`)
    }

    try {
      if (this.project.webConfig?.staticDir) {
        if (existsSync(this.project.webConfig.staticDir)) {
          await emptyDir(this.project.webConfig.staticDir)
          await copy(`${this.localPath}/public`, this.project.webConfig.staticDir)
        } else {
          throw new Error('Dir not exists')
        }
      }
    } catch (e) {
      console.log(e)
      throw new Error(`Error deploying to staticDir ${this.project.webConfig?.staticDir}.`)
    }
  }

  public async deploy (): Promise<void> {
    switch (this.project.type) {
      case 'strapi':
        await this.buildStrapiProject()
        await this.deployPm2Project()
        break
      case 'gatsby':
        await this.buildGatsbyProject()
        break
      default:
        throw new Error(`Invalid project type ${this.project.type}`)
    }
  }
}

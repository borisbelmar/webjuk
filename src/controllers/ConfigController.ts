import { readFileSync } from 'fs'
import { Config } from '../models'
import getUserHome from '../utils/getUserHome'

export default class ConfigController {
  public static getConfigFile () : Promise<Config> {
    const CONFIG_PATH = process.env.CONFIG_PATH || getUserHome()
    const CONFIG_FILENAME = process.env.CONFIG_FILENAME || '.webjuk.conf.json'
    const CONFIG_FULL_PATH = `${CONFIG_PATH}/${CONFIG_FILENAME}`

    try {
      const configFileBuffer = readFileSync(CONFIG_FULL_PATH)
      const configString = configFileBuffer.toString('utf-8')
      return JSON.parse(configString)
    } catch {
      throw new Error(`Fatal error: Check your config file, must be a valid JSON object: ${CONFIG_FULL_PATH}`)
    }
  }
}

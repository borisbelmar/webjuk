import { execSync } from 'child_process'

export default function getUserHome (): string {
  return String(execSync('eval echo ~$USER')).replace(/[\n\t\r]/g, '')
}

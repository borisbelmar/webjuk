import Router from '@koa/router'
import { find, propEq } from 'ramda'
import { ConfigController, DeployController, GitController } from '../controllers'
import { Project } from '../models'

const router = new Router()

const findProjectByName = (projectName: string, projects: Project[]) => (
  find<Project>(propEq('name', projectName))(projects)
)

router.post('/deploy/:projectName', async ctx => {
  const { projectName } = ctx.params
  const { secretkey } = ctx.query

  try {
    const config = await ConfigController.getConfigFile()

    if (config.secretKey !== secretkey) {
      ctx.throw(403, 'The secret key is incorrect')
    }

    const project = findProjectByName(projectName, config.projects)

    if (project) {
      const localPath = `${config.baseDir}/${project.name}`

      const gitController = new GitController(project, localPath)
      await gitController.cloneOrPullProject()

      const deployController = new DeployController(project, localPath)
      await deployController.deploy()

      ctx.body = `Successfull deploy for ${project.name}`
    } else {
      ctx.throw(404, 'Project not found')
    }
  } catch (e) {
    ctx.throw(500, e.message)
  }
})

export default router

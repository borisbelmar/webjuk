interface WebConfig {
  staticDir?: string
}

export interface Project {
  name: string
  user: string
  token: string
  repo: string
  type: 'strapi' | 'gatsby' | 'koa-ts' | 'koa' | 'react'
  branch?: string
  env?: { key: string }
  webConfig?: WebConfig
}

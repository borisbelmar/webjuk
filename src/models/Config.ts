import { Project } from './Project'

export interface Config {
  version: string
  baseDir: string
  secretKey: string
  projects: Array<Project>
}

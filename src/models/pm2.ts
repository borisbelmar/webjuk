/* eslint-disable camelcase */
export interface App {
  name: string
  script: string
  cwd?: string
  args?: string
  interpreter?: string
  interpreter_args?: string
  node_args?: string
  env?: { key: string }
}

export interface EcosystemFile {
  apps: App[]
}
